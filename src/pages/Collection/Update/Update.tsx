/* eslint-disable @typescript-eslint/no-explicit-any */
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { ICollection } from "../../../../common/interfaces/Collection";
import Message from "../../../../components/base/Message/Message";
import { Input } from "../../../../components/ui/Input";
import MultipleSelect from "../../../../components/ui/MultipleSelect";
import { ICategory } from "../../../../common/interfaces/Category";
import { update } from "../../../../services/collection";
interface UpdateComponentProps {
  id?: string;
  data: ICollection[];
  categoryData?: ICategory[];
}
const UpdateComponent = ({ id, data, categoryData }: UpdateComponentProps) => {
  const queryClient = useQueryClient();
  const [showMessage, setShowMessage] = useState(false);
  const [category, setCategory] = useState<ICategory[]>();
  const [selectedCategories, setSelectedCategories] = useState<string[]>([]);
  const handleCategorySelect = (selectedCategory: ICategory[]) => {
    const getID = selectedCategory
      .map((category) => category._id)
      .filter((id) => id !== undefined) as string[];
    setSelectedCategories(getID);
  };
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    setValue,
  } = useForm();

  const mutation = useMutation({
    mutationFn: async (collection: ICollection) => {
      const { data } = await update(collection);
      return data;
    },
    onSuccess: async () => {
      setShowMessage(true);
      queryClient.invalidateQueries({
        queryKey: ["COLLECTION_KEY"],
      });
    },
  });
  const onSubmit = async (data: ICollection) => {
    mutation.mutate(data);
    console.log(data);
  };
  useEffect(() => {
    setValue("category", selectedCategories);
  }, [selectedCategories]);
  useEffect(() => {
    const findDataById = data.find((data: ICollection) => data._id === id);
    if (!findDataById) return;
    setCategory(findDataById.category);
    reset(findDataById);
  }, [data, id, reset]);
  useEffect(() => {
    if (showMessage) {
      setTimeout(() => {
        setShowMessage(false);
      }, 3000);
    }
  }, [showMessage]);
  return (
    <div>
      <Message
        message={"Sửa nhãn hàng thành công !"}
        timeout={3000}
        openMessage={showMessage}
        type={"success"}
      />
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="space-y-12">
          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-base font-semibold leading-7 text-gray-900">
              Sửa nhãn hàng
            </h2>
            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
              <div className="sm:col-span-2">
                <label
                  htmlFor="productName"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Tên nhãn hàng
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <Input
                      type="text"
                      placeholder="Nhập tên nhãn hàng..."
                      {...register("name", { required: true })}
                    />
                  </div>
                  <p>
                    {errors.name && <span>Vui lòng không được để trống</span>}
                  </p>
                </div>
              </div>
              <div className="sm:col-span-2">
                <label
                  htmlFor="productName"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Danh mục
                </label>
                <div className="mt-2">
                  <MultipleSelect
                    data={categoryData}
                    onCategorySelect={handleCategorySelect}
                    categorySelect={category}
                  />
                  <input
                    className="hidden"
                    {...register("category", { required: true })}
                  />
                </div>
                <p>
                  {errors.category && <span>Vui lòng không được để trống</span>}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-6 flex items-center justify-end gap-x-6">
          <button className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
            Xác nhận
          </button>
        </div>
      </form>
    </div>
  );
};

export default UpdateComponent;
