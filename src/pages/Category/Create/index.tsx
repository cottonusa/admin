// SwipeableTemporaryDrawer.tsx
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import * as React from "react";
import CreateComponent from "./Create";
import { X } from "lucide-react";

type Anchor = "bottom";

export default function CategoryCreate() {
  const [state, setState] = React.useState({
    bottom: false,
  });
  const [isUpdateComponentVisible, setIsUpdateComponentVisible] =
    React.useState(false);

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
      if (open) {
        setIsUpdateComponentVisible(true);
      } else {
        setIsUpdateComponentVisible(false);
      }
    };
  const anchor: Anchor = "bottom";
  return (
    <div>
      <Button
        sx={{ color: "inherit", padding: 0, minWidth: "auto" }}
        onClick={toggleDrawer(anchor, true)}
      >
        Thêm danh mục
      </Button>
      <SwipeableDrawer
        id="drawer"
        anchor={anchor}
        open={state[anchor]}
        onClose={toggleDrawer(anchor, false)}
        onOpen={toggleDrawer(anchor, true)}
      >
        <Box className="p-5 m-4 relative" sx={{ height: 260 }}>
          <div
            className="absolute top-0 right-0 cursor-pointer"
            onClick={toggleDrawer(anchor, false)}
          >
            <X />
          </div>
          {isUpdateComponentVisible && <CreateComponent />}
        </Box>
      </SwipeableDrawer>
    </div>
  );
}
