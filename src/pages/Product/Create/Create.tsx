/* eslint-disable @typescript-eslint/no-explicit-any */
import { ChangeEvent, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import useCollectionQuery from "../../../../common/hooks/Collection/useCollectionQuery";
import { ICategory } from "../../../../common/interfaces/Category";
import { ICollection } from "../../../../common/interfaces/Collection";
import { IProduct } from "../../../../common/interfaces/Product";
import Loading from "../../../../components/base/Loading/Loading";
import { Input } from "../../../../components/ui/Input";
import ClassificationGroups from "./ClassificationGroups";
import "./style.css";
import { uploadFileCloudinary } from "../../../../systems/utils/uploadImage";
import Message from "../../../../components/base/Message/Message";
import useProductMutation from "../../../../common/hooks/Product/useProductMutation";

const ProductCreate = () => {
  const [showMessage, setShowMessage] = useState(false);
  const { data: collection } = useCollectionQuery();
  const [category, setCategory] = useState<ICategory[]>();
  const [selectedCollection, setSelectedCollection] = useState<string>("");
  const handleCollectionChanged = (event: ChangeEvent<HTMLSelectElement>) => {
    setSelectedCollection(event.target.value);
  };
  useEffect(() => {
    if (!selectedCollection) return;
    const getCategory = collection.find(
      (i: ICollection) => i._id === selectedCollection
    );
    if (getCategory) {
      setCategory(getCategory.category);
    }
  }, [selectedCollection, collection]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IProduct>();
  const { onSubmit, isPending } = useProductMutation({
    action: "CREATE",
  });
  const [uploadedImages, setUploadedImages] = useState<{ [key: string]: string }>({});

  const handleSubmitForm = async (formData: IProduct) => {
    try {
      // Lưu trữ public ID của các ảnh đã upload
      const uploadedImageIds: { [key: string]: string } = {};

      for (const variant of formData.variants) {
        const uploadPromises = Array.from(variant.thumbnail).map((file: any) =>
          uploadFileCloudinary(file)
            .then((data) => {
              uploadedImageIds[variant.id] = data;
              return data;
            })
            .catch((error) => {
              console.error('Error uploading file:', error);
              return null;
            })
        );
        const uploadedUrls = await Promise.all(uploadPromises);
        variant.thumbnail = uploadedUrls.filter(Boolean).map((data) => ({
          imageUrl: data
        }));
      }

      setUploadedImages(uploadedImageIds);

      const res = await onSubmit(formData);
      if (res) {
        setShowMessage(true);
      }
    } catch (error) {
      console.error('Error creating product:', error);
      // Xóa các ảnh đã upload nếu thêm sản phẩm thất bại
      await Promise.all(
        Object.values(uploadedImages).map((publicId) =>
          deleteImageFromCloudinary(publicId)
        )
      );
      // Hiển thị thông báo lỗi cho người dùng
    }
  };

  return (
    <>
      {!collection ? (
        <Loading />
      ) : (
        <div>
          <Message
            message={"Thêm sản phẩm thành công !"}
            timeout={5000}
            openMessage={showMessage}
            type={"success"}
          />
          <form onSubmit={handleSubmit(handleSubmitForm)}>
            <div className="space-y-12">
              <div className="border-b border-gray-900/10 pb-12">
                <h2 className="text-base font-semibold leading-7 text-gray-900">
                  Thêm sản phẩm
                </h2>
                <p className="mt-1 text-sm leading-6 text-gray-600">
                  This information will be displayed publicly so be careful what
                  you share.
                </p>

                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                  <div className="sm:col-span-full">
                    <label
                      htmlFor="productName"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Tên sản phẩm
                    </label>
                    <div className="mt-2">
                      <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 w-full">
                        <Input
                          type="text"
                          placeholder="Nhập tên sản phẩm..."
                          {...register("name", { required: true })}
                        />
                      </div>
                      <p>
                        {errors.name && (
                          <span>Vui lòng không được để trống</span>
                        )}
                      </p>
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="SKU"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      SKU
                    </label>
                    <div className="mt-2">
                      <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                        <Input
                          type="text"
                          placeholder="Nhập SKU sản phẩm..."
                          {...register("sku", { required: true })}
                        />
                      </div>
                      <p>
                        {errors.sku && (
                          <span>Vui lòng không được để trống</span>
                        )}
                      </p>
                    </div>
                  </div>

                  <div className="sm:col-span-2">
                    <label
                      htmlFor="price"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Giá
                    </label>
                    <div className="mt-2">
                      <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                        <Input
                          type="text"
                          placeholder="Nhập giá sản phẩm..."
                          {...register("price", { required: true })}
                        />
                      </div>
                      <p>
                        {errors.price && (
                          <span>Vui lòng không được để trống</span>
                        )}
                      </p>
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="discount"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Giảm giá
                    </label>
                    <div className="mt-2">
                      <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                        <Input
                          type="text"
                          placeholder="Nhập giá sau khi giảm..."
                          {...register("discount", { required: false })}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="productName"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Chất liệu
                    </label>
                    <div className="mt-2">
                      <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                        <Input
                          type="text"
                          placeholder="Nhập chất liệu..."
                          {...register("material", { required: true })}
                        />
                      </div>
                      <p>
                        {errors.material && (
                          <span>Vui lòng không được để trống</span>
                        )}
                      </p>
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="Brand"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
                    >
                      Brand
                    </label>
                    {/* <input type="text" className="" /> */}
                    <select
                      className="relative cursor-pointer w-full sm:max-w-sm rounded-md bg-white py-3 pl-3 pr-10 text-left text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6"
                      // value={selectedCollection}
                      {...register("collections", { required: true })}
                      onChange={handleCollectionChanged}
                    >
                      <option value="">Chọn một brand...</option>
                      {collection.map((v: ICollection, i: number) => (
                        <option key={i} value={v._id}>
                          {v.name}
                        </option>
                      ))}
                    </select>
                    <p>
                      {errors.collections && (
                        <span>Vui lòng không được để trống</span>
                      )}
                    </p>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="Category"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
                    >
                      Kiểu áo
                    </label>
                    <select
                      className="relative cursor-pointer w-full sm:max-w-sm rounded-md bg-white py-3 pl-3 pr-10 text-left text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6"
                      {...register("category", { required: true })}
                    >
                      {category ? (
                        category.length === 0 ? (
                          <option value="">Brand chưa có danh mục</option>
                        ) : (
                          <>
                            <option value="">Chọn kiểu áo...</option>
                            {category.map((v: ICollection, i: number) => (
                              <option key={i} value={v._id}>
                                {v.name}
                              </option>
                            ))}
                          </>
                        )
                      ) : (
                        <option value="">Vui lòng chọn brand trước</option>
                      )}
                    </select>
                    <p>
                      {errors.category && (
                        <span>Vui lòng không được để trống</span>
                      )}
                    </p>
                  </div>

                  <div className="col-span-full">
                    <label
                      htmlFor="about"
                      className="block text-sm font-medium leading-6 text-gray-900"
                    >
                      Mô tả sản phẩm
                    </label>
                    <div className="mt-2">
                      <textarea
                        rows={3}
                        className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        {...register("description", { required: true })}
                      />
                    </div>
                    <p>
                      {errors.description && (
                        <span>Vui lòng không được để trống</span>
                      )}
                    </p>
                  </div>
                </div>

                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                  <ClassificationGroups register={register} errors={errors} />
                </div>
              </div>
            </div>
            <div className="mt-6 flex items-center justify-end gap-x-6">
              <Link to="/admin/product">
                <button
                  type="button"
                  className="text-sm font-semibold leading-6 text-gray-900"
                >
                  Cancel
                </button>
              </Link>
              <button
                type="submit"
                className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                {isPending ? "Đang thêm" : "Thêm"}
              </button>
            </div>
          </form>
        </div>
      )}
    </>
  );
};

export default ProductCreate;
