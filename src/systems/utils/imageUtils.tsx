import axios from "axios";
import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}
const uploadFileCloudinary = async (file: File) => {
  try {
    const formData = new FormData();
    formData.append("file", file);
    formData.append("upload_preset", "cottonUSA");
    formData.append("folder", "cottonUSA");
    const response = await axios.post(
      "https://api.cloudinary.com/v1_1/dou8bu6ei/upload",
      formData
    );
    return response.data.url;
  } catch (error) {
    console.error(error);
  }
};

export { uploadFileCloudinary };
