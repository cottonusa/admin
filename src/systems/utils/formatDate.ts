/* eslint-disable @typescript-eslint/no-explicit-any */
import { format } from "date-fns";

export const formatDate = (dateString: any) => {
  const date = new Date(dateString);
  return format(date, "HH:mm dd/MM/yyyy");
};
