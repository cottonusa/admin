import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import LayoutWebsite from "../layouts/Layouts";
import Dashboard from "../pages/Dashboard/Dashboard";
import ProductList from "../pages/Product/List/List";
import ProductCreate from "../pages/Product/Create/Create";
import ProductUpdate from "../pages/Product/Update/Update";
import Category from "../pages/Category/List/List";
import Collection from "../pages/Collection/List/List";
const RouterComponent = () => {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/admin" element={<LayoutWebsite />}>
            <Route index element={<Dashboard />} />
            <Route path="/admin/product" element={<ProductList />} />
            <Route path="/admin/product/create" element={<ProductCreate />} />
            <Route
              path="/admin/product/update/:id"
              element={<ProductUpdate />}
            />
            <Route path="/admin/category" element={<Category />} />
            <Route path="/admin/collection" element={<Collection />} />
          </Route>
        </Routes>
      </Router>
    </>
  );
};

export default RouterComponent;
