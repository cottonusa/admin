import { useQuery } from "@tanstack/react-query";
import { get, getBySlug } from "../../../services/product";
type Props = {
  id?: string;
  page?: number;
  limit?: number;
  slug?: string;
};
const useProductQuery = ({ slug, page, limit }: Props = {}) => {
  const { data, ...rest } = useQuery({
    queryKey: ["PRODUCT_KEY", slug],
    queryFn: async () => {
      return slug ? await getBySlug(slug) : await get(page, limit);
    },
  });
  return { data, ...rest };
};
export default useProductQuery;
