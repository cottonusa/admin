import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import RouterComponent from "./router/route";
const App = () => {
  return (
    <>
      <RouterComponent />
      <ToastContainer />
    </>
  );
};
export default App;
