import Joi from "joi";

export const ProductJoiSchema = Joi.object({
  name: Joi.string().required().empty().messages({
    "string.base": `Vui lòng nhập tên sản phẩm`,
    "string.empty": `Vui lòng nhập tên sản phẩm`,
    "any.required": `Vui lòng nhập tên sản phẩm`,
  }),
  description: Joi.string().required().max(2500).messages({
    "string.base": `Mô tả phải là kiểu 'văn bản'`,
    "string.empty": `Mô tả không thể là trường trống`,
    "any.required": `Mô tả là một trường bắt buộc`,
    "string.max": "Mô tả sản phẩm không được quá {#limit} ký tự",
  }),
  price: Joi.number().required().min(0).messages({
    "number.base": `Vui lòng nhập giá sản phẩm`,
    "number.empty": `Vui lòng nhập giá sản phẩm`,
    "any.required": `Vui lòng nhập giá sản phẩm`,
    "number.min": "Giá không thể là số ấm",
  }),
  material: Joi.string().required().messages({
    "string.base": `Chất liệu phải là kiểu 'văn bản'`,
    "string.empty": `Chất liệu không thể là trường trống`,
    "any.required": `Chất liệu là một trường bắt buộc`,
  }),
  discount: Joi.number().optional().allow("").messages({
    "number.base": `Giảm giá phải là kiểu 'văn bản'`,
  }),
  variants: Joi.array()
    .items(
      Joi.object({
        color: Joi.string().required().messages({
          "string.base": `Màu sắc phải là kiểu 'văn bản'`,
          "string.empty": `Màu sắc không thể là trường trống`,
          "any.required": `Màu sắc là một trường bắt buộc`,
        }),
        thumbnail: Joi.array()
          .items(
            Joi.object({
              imageUrl: Joi.string().uri().required().messages({
                "string.base": `URL hình ảnh phải là kiểu 'văn bản'`,
                "string.empty": `URL hình ảnh không thể là trường trống`,
                "string.uri": `URL hình ảnh phải là một URI hợp lệ`,
                "any.required": `URL hình ảnh là một trường bắt buộc`,
              }),
              isBackground: Joi.boolean().required().messages({
                "boolean.base": `Có phải nền không phải là kiểu 'boolean'`,
                "any.required": `Có phải nền không là một trường bắt buộc`,
              }),
            })
          )
          .required()
          .messages({
            "array.base": `Thumbnail phải là một mảng`,
            "any.required": `Thumbnail là một trường bắt buộc`,
          }),
        option: Joi.array()
          .items(
            Joi.object({
              size: Joi.string().required().messages({
                "string.base": `Kích thước phải là kiểu 'văn bản'`,
                "string.empty": `Kích thước không thể là trường trống`,
                "any.required": `Kích thước là một trường bắt buộc`,
              }),
              quantity: Joi.number().integer().required().messages({
                "number.base": `Số lượng phải là kiểu 'số'`,
                "number.integer": `Số lượng phải là một số nguyên`,
                "any.required": `Số lượng là một trường bắt buộc`,
              }),
            })
          )
          .required()
          .messages({
            "array.base": `Tùy chọn phải là một mảng`,
            "any.required": `Tùy chọn là một trường bắt buộc`,
          }),
      })
    )
    .required()
    .messages({
      "array.base": `Biến thể phải là một mảng`,
      "any.required": `Biến thể là một trường bắt buộc`,
    }),
  sku: Joi.string().required().messages({
    "string.base": `SKU phải là kiểu 'văn bản'`,
    "string.empty": `SKU không thể là trường trống`,
    "any.required": `SKU là một trường bắt buộc`,
  }),
  collections: Joi.string().required().messages({
    "string.base": `Bộ sưu tập phải là kiểu 'văn bản'`,
    "string.empty": `Bộ sưu tập không thể là trường trống`,
    "any.required": `Bộ sưu tập là một trường bắt buộc`,
  }),
  category: Joi.string().required().messages({
    "string.base": `Danh mục phải là kiểu 'văn bản'`,
    "string.empty": `Danh mục không thể là trường trống`,
    "any.required": `Danh mục là một trường bắt buộc`,
  }),
});
